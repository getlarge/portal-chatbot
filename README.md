# Hubot Broker

**portal-chatbot** is a **Mosca - MQTT.js** broker - client, handling messages and events from bots ( **Hubot** ), which works in conjunction with [chat-bot](https://framagit.org:getlarge/live_assistant) and [broka-billy](https://framagit.org:getlarge/broka-billy).


## Table of Contents
- [Features](#features)
- [Setup](#setup)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)

## Features
### Broker
+ receives MQTT messages from Hubot ( chatbot protocol )
+ forwards the received messages to the master MQTT broker


## Setup
### Prerequisites

+ node.js
+ npm

Once installed in your machine, you can simply run the commands listed in the steps below.


## Installation

To get started with this project, follow the 3-step installation, described here.

### 1. portal-chatbot *broker* 

Navigate inside the root folder and run:

```
npm install
npm start
```
this will install the dependencies and start the Mosca broker locally on port 1884.


## Usage
### Deploy

Start with connector-lorawan.json.sample to configure your broker instance and save the file as broka-billy.json
Then init with : 

```
npm install -g pm2
pm2 start connector-lorawan.json
```

## Built With
### Broker
* [Mosca](https://github.com/mcollina/mosca) - MQTT broker for Node.js 
* [MQTT.js](https://github.com/mqttjs/MQTT.js) - The MQTT client for Node.js and the browser

## Contributing

Please contribute using [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). Create a feature branch, add commits, and [open a pull request].
