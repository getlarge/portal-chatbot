var client = require('../config/mqtt');
var mqttServer = require('./mqttServer');
var conf = require('../config/config');


client.on("connect", () => {
    console.log(conf.appname + " client connected @" + conf.client.url);
    //client.subscribe('logs/#');
});

client.on("message", (topic, message) => {
    const payload = message.toString();
    console.log(conf.appname + "received :", payload + "from" + topic);

    var pattern = "+bot/+command";
    var params = { bot: "Camera2894413", command: "in" }
    var testTopic = mqttPattern.filled(pattern, params);
    console.log("testTopic", testTopic);

    //topic = conf.prefix + "/" + params.gw + "/" + params.command;
    mqttServer.send(topic, payload, {qos: 1});


});
