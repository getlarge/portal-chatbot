var mosca = require('mosca');
var mqttPattern = require("mqtt-pattern");
var mqttClient = require('../config/mqtt');
var moment = require('moment');
var conf = require('../config/config');

function mqttServer() {
    var authenticate = (client, username, password, callback) => {
        if (username == conf.broker.username && password.toString() == conf.broker.password )
            callback(null, true);
        else
            callback(null, false);
    }

    var authorizePublish = (client, topic, payload, callback) => {
        var auth = true;
        // set auth to :
        //  true to allow 
        //  false to deny and disconnect
        //  'ignore' to puback but not publish msg.
        callback(null, auth);
    }

    var authorizeSubscribe = (client, topic, callback) => {
        var auth = true;
        // set auth to :
        //  true to allow
        //  false to deny 
        callback(null, auth);
    }

    broker = new mosca.Server(conf.broker);

    function setup() {
        broker.authenticate = authenticate;
        broker.authorizePublish = authorizePublish;
        broker.authorizeSubscribe = authorizeSubscribe;
        console.log( conf.appname + " broker ready, MQTT running @ " + conf.broker.interfaces[0].port);
        //console.log( conf.appname + " broker ready, MQTT running @ " + conf.broker.interfaces[0].port, "and WS running @ " + conf.broker.interfaces[1].port);
    }

    broker.on("ready", setup);

    broker.on('clientConnected', (client) => {
        var timestamp = moment().format('LTS');
        console.log("Client Connected:", client.id, "at", timestamp);
        mqttClient.send( conf.prefix + "/stat/" + client.id + "/connected", "1");
    });

    broker.on('clientDisconnected', (client) => {
        var timestamp = moment().format('LTS');
        console.log("Client Disconnected: ", client.id, "at", timestamp);
        mqttClient.send( conf.prefix + "/stat/" + client.id + "/connected", "0");
    });

    broker.on('published', (packet, client) => {
    	var timestamp = moment().valueOf();
        var topic = conf.prefix + "/" + packet.topic;
        mqttClient.send(topic, packet.payload.toString(), {qos: 1});

        var pattern = "+bot/+command";
        if ( mqttPattern.matches(pattern, topic)) {
            var params = mqttPattern.extract(pattern, topic);
            topic = conf.prefix + "/" + params.bot + "/" + params.command;
            mqttClient.send(topic, packet.payload.toString(), {qos: 1});
        }
    });
}

mqttServer.prototype.send = (topic, message) => {

    broker.publish({
        topic: topic,
        payload: message,
        retain: false,
        qos: 1
    });
}

module.exports = new mqttServer();
