const appName = Number(process.env.APP_NAME) || "Portal Chatbot";
const prefix = Number(process.env.PREFIX) || "chatbot";
const brokerPort = Number(process.env.BROKER_PORT) || 4003;
const serverPort = Number(process.env.SERVER_PORT) || 3003;
const clientUrl = process.env.CLIENT_URL || "mqtt://localhost:1884";
const clientHost = process.env.CLIENT_HOST || "localhost";
const clientPort = Number(process.env.CLIENT_PORT) || 1884;
const clientId = process.env.CLIENT_ID || "portal-chatbot";
const user =  process.env.CLIENT_USER || "";
const pass = process.env.CLIENT_PASS || "";

var config = {
    appname: appName,
    prefix: prefix,
    client: {
        url: clientUrl,
        options: {
            host: clientHost,
            port: clientPort,
            clientId: clientId + "_" + Math.random().toString(16).substr(2, 8),
            username: user,
            password: new Buffer(pass)
        }
    },
    broker: {
        username: user,
        password: pass,
        interfaces: [
            { type: "mqtt", port: brokerPort },
            //{ type: "mqtts", port: brokerPortSecure, credentials: { keyPath: SECURE_KEY, certPath: SECURE_CERT } },
            //{ type: "http", port: serverPort, bundle: true, static: './' }
            //{ type: "https", port: serverPortSecure, bundle: true, credentials: { keyPath: SECURE_KEY, certPath: SECURE_CERT } }
        ],
    }
};

try {

    var configCustom = require('./config.custom.js');

} catch (e) {

    var configCustom = {};
}

module.exports = Object.assign({}, config, configCustom);
