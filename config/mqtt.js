var mqtt = require('mqtt');
var conf = require('./config');

var client = mqtt.connect(conf.client.url, conf.client.options);

client.send = (topic, message) => {

    client.publish(topic, message, {
        retain: false,
        qos: 1
    });
}

module.exports = client;
